namespace PluginBase
{
    public interface IPlugin
    {
        string Name { get; }
        string Description { get; }
        string ResultMessage { get; }
        string Execute(string input);
    }
}