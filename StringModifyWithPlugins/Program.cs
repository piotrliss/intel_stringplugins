﻿using PluginBase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace StringModifyWithPlugins
{
    class Program
    {
        static void Main(string[] args)
        {
            var inputString = System.String.Empty;
            try
            {
                if (args.Length == 1 && args[0] == "/d")
                {
                    Console.WriteLine("Waiting for any key...");
                    Console.ReadLine();
                }

                string[] pluginPathFiles = Directory.GetFiles(@"..\Plugins\", "*.dll");

                IEnumerable<IPlugin> commands = pluginPathFiles.SelectMany(pluginPath =>
                {
                    Assembly pluginAssembly = LoadPlugin(pluginPath);
                    return CreateCommands(pluginAssembly);
                }).ToList();

                if (args.Length == 0)
                {
                    Console.WriteLine("Commands: ");
                    foreach (IPlugin command in commands)
                        {
                            Console.WriteLine($"{command.Name}\t - {command.Description}");
                        }
                }
                else
                {
                    Console.Write("Provide input string: ");
                    inputString = Console.ReadLine();

                    foreach (string commandName in args)
                    {
                        IPlugin command = commands.FirstOrDefault(c => c.Name == commandName);
                        if (command == null)
                        {
                            Console.WriteLine("No such command is known.");
                            return;
                        }

                        var response = command.Execute(inputString);
                        Console.WriteLine($"You entered the phrase: \"{inputString}\"");
                        Console.WriteLine($"{command.ResultMessage} {response}");
                        Console.WriteLine();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            static Assembly LoadPlugin(string relativePath)
            {
                string root = System.IO.Directory.GetCurrentDirectory();
                string pluginLocation = Path.GetFullPath(Path.Combine(root, relativePath.Replace('\\', Path.DirectorySeparatorChar)));
                Console.WriteLine($"Loading commands from: {pluginLocation}");
                PluginLoadContext loadContext = new PluginLoadContext(pluginLocation);
                return loadContext.LoadFromAssemblyName(new AssemblyName(Path.GetFileNameWithoutExtension(pluginLocation)));
            }

            static IEnumerable<IPlugin> CreateCommands(Assembly assembly)
            {
                int count = 0;

                foreach (Type type in assembly.GetTypes())
                {
                    if (typeof(IPlugin).IsAssignableFrom(type))
                    {
                        IPlugin result = Activator.CreateInstance(type) as IPlugin;
                        if (result != null)
                        {
                            count++;
                            yield return result;
                        }
                    }
                }

                if (count == 0)
                {
                    string availableTypes = string.Join(",", assembly.GetTypes().Select(t => t.FullName));
                    throw new ApplicationException(
                        $"Can't find any type which implements IPlugin in {assembly} from {assembly.Location}.\n" +
                        $"Available types: {availableTypes}");
                }
            }
        }
    }
}