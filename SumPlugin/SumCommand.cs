﻿using PluginBase;
using System;

namespace SumPlugin
{
    public class SumCommand : IPlugin
    {
        public string Name { get => "SumParsedCharacters"; }
        public string Description { get => "Sum characters that can be parsed to integer value."; }
        public string ResultMessage { get => "Sum of integer values is:"; }
        public string Execute(string input) {
            char[] characters = input.ToCharArray();
            int sumOfParsedString = 0;
            int parsedNumber;
            foreach (char character in characters) {
                if (Int32.TryParse(character.ToString(), out parsedNumber)) {
                    sumOfParsedString += parsedNumber;
                }
            }
            return sumOfParsedString.ToString();
        }
    }
}