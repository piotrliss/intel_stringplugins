## Author

Piotr Liss

## Description

Solution for technical question in intel recruitment process for "Software Developer" role.

## Structure

```
netCore_stringPlugins
├── CountPlugin
│   └── CountCommand.cs
├── Plugins
│   └── CountPlugin.dll
│   └── ReversePlugin.dll
│   └── SumPlugin.dll
├── PluginBase
│   └── IPlugin.cs
├── ReversePlugin
│   └── ReverseCommand.cs
├── StringModifyWithPlugins
│   ├── PluginLoadContext.cs
│   └── Program.cs
└── SumPlugin
    └── SumCommand.cs
```
#### Files content:
- `Program.cs` - running CLI app + initializing, running and showing available plugins
- `PluginLoadContext.cs` - loads plugin assemblies and resolves dependencies
- `Plugins` - directory with plugins library
- `IPlugin.cs` - provided plugin interface template
- `CountCommand.cs` - plugin that returns number of characters
- `ReverseCommand.cs` - plugin that returns reversed input string
- `SumCommand.cs` - plugin that returns sum of characters that can be parsed to integer

## How to run

### List of implemented plugins
Run CLI application from `StringModifyWithPlugins` directory with no arguments:
```dotnet
dotnet run
```
as a result we receive
```
Commands:
CountCharacters          - Counting given string number of characters.
SumParsedCharacters      - Sum characters that can be parsed to integer value.
ReverseCharacters        - Reverse characters of given string.
```

### Run a plugin
Run CLI application from `StringModifyWithPlugins` directory with argument containing plugin name, e.g.:
```dotnet
dotnet run CountCharacters
```
as a result we receive:
```
Loading commands from: ...\CountPlugin.dll
Provide input string: w9ftu9a84g;dfsfgs;lsd
You entered the phrase: "w9ftu9a84g;dfsfgs;lsd"
Number of characters: 21
```

## How to add plugin

Simply add DLL library file into `Plugins` folder.

## Original task content
**Implement a plugginable console application in .NET Core 3.1 with plugin discovery.**
### Requirements:
Plugins should implement the following interface:
```dotnet
public interface IPlugin
{
   string Execute(string input);
}
```

For example: ReversePlugin (reverses input and returns it), CountPlugin (counts characters in input string and returns the number as string), SumPlugin (splits string by ‘+’ sign, parses number, calculates sum of them and returns it as a string)

* Plugins should support different instance lifecycle settings, for example: new instance per each plugin call, single instance per all plugin calls (throughout application lifecycle)
* Plugins should be automatically discovered, i.e. you are able to add a new assembly to an appropriate directory and the plugin is instantly available for use
* Provide CLI for the application, available scenarios:
  * List all available plugins with descriptions
  * Execute a selected plugin with a given input string
  * Interactive mode, where you can interact with plugins in a simple scripted manner (like interactive Python interpreter).

### Additional information:

* Propose an appropriate architecture. If required, you can use additional components
* Take care of proper testing of the application, at different levels,
* Provide example plugins which demonstrate possibilities of your proposed architecture,
* Be creative – show your potential, there are many approaches how you can implement and demonstrate this architecture.

#### Optional features:

* Persistent storage of all actions executed in application – kind of activity log,
* Possibility to browse actions history via network.