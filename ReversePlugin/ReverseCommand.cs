﻿using PluginBase;
using System;

namespace ReversePlugin
{
    public class ReverseCommand : IPlugin
    {
        public string Name { get => "ReverseCharacters"; }
        public string Description { get => "Reverse characters of given string."; }
        public string ResultMessage { get => "Reversed phrase is:"; }
        public string Execute(string input) {
            char[] characters = input.ToCharArray();
            Array.Reverse(characters);
            return String.Concat(characters);
        }
    }
}