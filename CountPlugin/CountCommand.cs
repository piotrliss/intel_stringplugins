﻿using PluginBase;
using System;

namespace CountPlugin
{
    public class CountCommand : IPlugin
    {
        public string Name { get => "CountCharacters"; }
        public string Description { get => "Counting given string number of characters."; }
        public string ResultMessage { get => "Number of characters:"; }
        public string Execute(string input) {
            return input.Length.ToString();
        }
    }
}